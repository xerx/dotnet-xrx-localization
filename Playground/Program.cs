﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;
using Xrx.Localization;
using Xrx.Localization.Abstractions;

namespace Playground
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            ILocalizationService s = new Loc();
            s.Initialize(new ExProvider()).Wait();
            Console.WriteLine(s.GetString("Hi"));
            s.CurrentCulture = Cultures.EL;
            Console.WriteLine(s.GetString("Hi"));
            Console.ReadLine();
        }

    }

    class SSource : IStringsProvider
    {
        public Task<Dictionary<object, ILocalizedString>> GetStrings()
        {
            return Task.Factory.StartNew(() =>
            {
                Dictionary<object, ILocalizedString> dict = new Dictionary<object, ILocalizedString>();
                dict["Hi"] = new LocalizedString
                {
                    {Cultures.EN, "Hello" },
                    {Cultures.EL, "Γειά σας" }
                };
                return dict;
            });
        }
    }
    class ExProvider : IStringsProvider
    {
        public Task<Dictionary<object, ILocalizedString>> GetStrings()
        {
            return Task.Run(() =>
            {
                Assembly assembly = IntrospectionExtensions.GetTypeInfo(typeof(MainClass)).Assembly;
                Stream stream = assembly.GetManifestResourceStream("Playground.strs.txt");
                string text = "";
                using (var reader = new StreamReader(stream))
                {
                    text = reader.ReadToEnd();
                }
                string[] a = text.Split('\n');
                Dictionary<object, ILocalizedString> d = new Dictionary<object, ILocalizedString>();
                for (int i = 0; i < a.Length; i++)
                {
                    string[] b = a[i].Split(',');
                    d[b[0]] = new LocalizedString
                    {
                        { Cultures.EN, b[1] },
                        { Cultures.EL, b[2] }
                    };
                }
                return d;
            });
        }
    }
    public static class Cultures
    {
        public static readonly CultureInfo EN = new CultureInfo("en");
        public static readonly CultureInfo EL = new CultureInfo("el");
    }
    public class Loc : BaseLocalizationService
    {
        private Dictionary<object, ILocalizedString> strings;
        public Loc()
        {
            _currentCulture = Cultures.EN;
        }
        public override string GetString(object stringId)
        {
            return strings[stringId].GetValue(CurrentCulture);
        }

        public override string GetString(object stringId, CultureInfo culture)
        {
            return strings[stringId].GetValue(culture);
        }

        public override async Task Initialize(IStringsProvider provider)
        {
            strings = await provider.GetStrings();
        }
    }
    public class LocalizedString : Dictionary<CultureInfo, string>, ILocalizedString
    {
        public string GetValue(CultureInfo culture)
        {
            return this[culture];
        }

        public void SetValue(CultureInfo culture, string value)
        {
            this[culture] = value;
        }
    }
}