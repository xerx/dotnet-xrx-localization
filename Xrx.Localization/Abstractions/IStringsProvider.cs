﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Xrx.Localization.Abstractions
{
    public interface IStringsProvider
    {
        Task<Dictionary<object, ILocalizedString>> GetStrings();
    }
}
