﻿using System.Globalization;

namespace Xrx.Localization.Abstractions
{
    public interface ILocalizedString
    {
        void SetValue(CultureInfo culture, string value);
        string GetValue(CultureInfo culture);
    }
}
