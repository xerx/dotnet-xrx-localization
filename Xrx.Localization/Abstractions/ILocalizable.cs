﻿using System.Globalization;
namespace Xrx.Localization.Abstractions
{
    public interface ILocalizable
    {
        void ChangeCulture(CultureInfo culture);
    }
}
