﻿using System.Globalization;
using System.Threading.Tasks;

namespace Xrx.Localization.Abstractions
{
    public interface ILocalizationService
    {
        Task Initialize(IStringsProvider provider);
        string GetString(object stringId);
        string GetString(object stringId, CultureInfo culture);
        void Register(ILocalizable localizable);
        void Unregister(ILocalizable localizable);
        CultureInfo CurrentCulture { get; set; }
    }
}
