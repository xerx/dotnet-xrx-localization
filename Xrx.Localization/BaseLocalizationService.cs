﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading.Tasks;
using Xrx.Localization.Abstractions;
namespace Xrx.Localization
{
    public abstract class BaseLocalizationService : ILocalizationService
    {
        protected List<ILocalizable> localizables = new List<ILocalizable>();

        protected CultureInfo _currentCulture;
        public virtual CultureInfo CurrentCulture 
        {
            get => _currentCulture;
            set
            {
                if (_currentCulture != null && _currentCulture.Equals(value)) { return; }
                _currentCulture = value;
                NotifyLocalizables();
            }
        }

        protected virtual void NotifyLocalizables()
        {
            for (int i = localizables.Count - 1; i > -1; i--)
            {
                if (localizables[i] != null)
                {
                    localizables[i].ChangeCulture(CurrentCulture);
                }
                else
                {
                    localizables.RemoveAt(i);
                }
            }
        }

        public abstract string GetString(object stringId);

        public abstract string GetString(object stringId, CultureInfo culture);

        public abstract Task Initialize(IStringsProvider provider);

        public virtual void Register(ILocalizable localizable)
        {
            if (localizables.Contains(localizable)) { return; }
            localizables.Add(localizable);
        }

        public virtual void Unregister(ILocalizable localizable)
        {
            if (!localizables.Contains(localizable)) { return; }
            localizables.Remove(localizable);
        }

    }
}
